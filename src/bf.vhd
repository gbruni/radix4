library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- butterfly for computing of DFT-4 points
-- it doesn't change the order of data in output
-- with respect to the input data order
-- 
-- data used: DATA_RDX
--
-- overflow not managed because
-- the datapath width was set
-- in such a way to avoid this phenomenon
--

entity BF is
	port (	A0, A1, A2, A3: in DATA_RDX;	-- input data
			Z0, Z1, Z2, Z3: out DATA_RDX	-- output data
			);
end BF;


architecture RTL of BF is

begin

--------- peculiar sum and subtraction operations of butterfly ---------
	Z0 <= (A0(1) + A1(1) + A2(1) + A3(1), A0(0) + A1(0) + A2(0) + A3(0));
	Z1 <= (A0(1) + A1(0) - A2(1) - A3(0), A0(0) - A1(1) - A2(0) + A3(1));
	Z2 <= (A0(1) - A1(1) + A2(1) - A3(1), A0(0) - A1(0) + A2(0) - A3(0));
	Z3 <= (A0(1) - A1(0) - A2(1) + A3(0), A0(0) + A1(1) - A2(0) - A3(1));

end RTL;

