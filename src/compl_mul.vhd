library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- generic (ROW) complex multiplier
-- it performs the multiplication
-- among the input data and
-- the twiddle factor related to
-- the position (column and row) of the input data
-- 
-- the various parts of the twiddle factor
-- (real part, sum Re+Im, difference Re-Im)
-- are all inside a ROM defined in the package radix4_pkg
--
-- there is an inner counter used for selecting
-- the column of the input data
--
-- signal CTR perform the reset of the counter
--
-- for selecting the row, just specify
-- the generic ROW
-- 		ROW = 1 => row 1
-- 		ROW = 2 => row 2
-- 		ROW = 3 => row 3
-- for the row 0 there is no specified coefficients
-- because twiddle factors of this line are all 1
--
-- there are 2 architectures
--		RTL64:	for the multipliers are
--				part of the DFT-64 points
--		RTL16:	for the multipliers are
--				part of the DFT-16 points
--
-- there is an overflow check at the end of every architecture.
-- however it is useless because twiddle factors are
-- complex exponentials => they perform only a rotation
-- of the input data (that are vectors in the complex plane).
-- so the Re part and Im part don't exceed the range of input data
--

entity COMPL_MUL is
	generic(ROW: natural);		-- row belonging
								-- of the multiplier
	port (	A0: in DATA_RDX;	-- input data
			CK: in std_logic;	-- clock signal
			CTR: in std_logic;	-- reset signal of counter
			Z0: out DATA_RDX	-- output data
			);
end COMPL_MUL;


---------------------------------- architecture of complex multipliers for 64 points-transform ----------------------------------
architecture RTL64 of COMPL_MUL is

	attribute keep : boolean;

	signal WR: signed(11 downto 0);					-- signal for storing the real part of twd_fc
													-- Re part [-1, +1] can be expressed using 12 signed-bit [-2047, +2047]
	signal WS, WD: signed(12 downto 0);				-- signals for storing the sum and the difference for twd_fc
													-- they can NOT be expressed using 12 signed-bit because the range is not necessarily [-1, +1]
	signal E: signed(29 downto 0);					-- temporary signals used in the various operations
	signal G1, G2, YRT, YIT: signed(30 downto 0);	-- temporary signals used in the various operations
	signal XR, XI, F: signed(A0(0)'range);			-- temporary signals used in the various operations
	signal YRO, YIO : signed(19 downto 0);			-- temporary signals used in the various operations
	signal CNT: unsigned(3 downto 0);				-- counter signal (from 0 to 15)
													-- it's restricted to 4 bit: in this way it can reach 15
													-- and then it reset itself ciclycally
													-- using not-managed overflow
	
	attribute keep of YRT : signal is true;
	attribute keep of YIT : signal is true;

begin
	WR <= conv_signed(TWDL64(ROW)(2)(conv_integer(CNT)), 12);	-- reading of Re of twdf
	WS <= TWDL64(ROW)(1)(conv_integer(CNT));					-- reading of sum Re+Im of twdf
	WD <= TWDL64(ROW)(0)(conv_integer(CNT));					-- reading of difference Re-Im of twdf
	XR <= A0(1);												-- Re of input data (better code reading)
	XI <= A0(0);												-- Im of input data (better code reading)

------ operations for product computing with range extensions etc for avoiding overflow ------
	F <= XR-XI;
	E <= WR*(F);

	G1 <= WD*XI;
	YRT <= G1 + conv_signed(E, 31);
	YRO <= YRT(30 downto 11);

	G2 <= WS*XR;
	YIT <= G2 - conv_signed(E, 31);
	YIO <= YIT(30 downto 11);

	Z0 <= (conv_signed (YRO, 18), conv_signed (YIO, 18));
	
	
	process(CK)	-- counter positive edge (16 values)
	begin
		if CK'event and CK='1' then
			if CTR='0' then				-- reset
				CNT <= conv_unsigned(0, CNT'length);
			else
				CNT <= CNT+1;			-- counter increasing
			end if;
		end if;
	end process;
	
end RTL64;


---------------------------------- architecture of complex multipliers for 16 points-transform ----------------------------------
architecture RTL16 of COMPL_MUL is

	attribute keep : boolean;
	
	
	signal WR: signed(11 downto 0);					-- signal for storing the real part of twd_fc
													-- Re part [-1, +1] can be expressed using 12 signed-bit [-2047, +2047]
	signal WS, WD: signed(12 downto 0);				-- signals for storing the sum and the difference for twd_fc
													-- they can NOT be expressed using 12 signed-bit because the range is not necessarily [-1, +1]
	signal E: signed(29 downto 0);					-- temporary signals used in the various operations
	signal G1, G2, YRT, YIT: signed(30 downto 0);	-- temporary signals used in the various operations
	signal XR, XI, F: signed(A0(0)'range);			-- temporary signals used in the various operations
	signal YRO, YIO : signed(19 downto 0);			-- temporary signals used in the various operations
	signal CNT: unsigned(1 downto 0);				-- counter signal (from 0 to 15)
													-- it's restricted to 4 bit: in this way it can reach 15
													-- and then it reset itself ciclycally
													-- using not-managed overflow
	
	attribute keep of YRT : signal is true;
	attribute keep of YIT : signal is true;

begin
	WR <= conv_signed(TWDL16(ROW)(2)(conv_integer(CNT)), 12);	-- reading of Re of twdf
	WS <= TWDL16(ROW)(1)(conv_integer(CNT));					-- reading of sum Re+Im of twdf
	WD <= TWDL16(ROW)(0)(conv_integer(CNT));					-- reading of difference Re-Im of twdf
	XR <= A0(1);												-- Re of input data (better code reading)
	XI <= A0(0);												-- Im of input data (better code reading)

------ operations for product computing with range extensions etc for avoiding overflow ------
	F <= XR-XI;
	E <= WR*(F);

	G1 <= WD*XI;
	YRT <= G1 + conv_signed(E, 31);
	YRO <= YRT(30 downto 11);

	G2 <= WS*XR;
	YIT <= G2 - conv_signed(E, 31);
	YIO <= YIT(30 downto 11);

	Z0 <= (conv_signed (YRO, 18), conv_signed (YIO, 18));
	
	
	process(CK)	-- counter positive edge (16 values)
	begin
		if CK'event and CK='1' then
			if CTR='0' then			-- reset
				CNT <= conv_unsigned(0, CNT'length);
			else
				CNT <= CNT+1;		-- counter increasing
			end if;
		end if;
	end process;
	
end RTL16;

