library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- entity for the entire processor.
-- it is made up of port map of
-- finite state machine and of
-- the computing part radix4.
-- moreover it performs a conversion
-- of bit range of input/output data

entity TOP is

	port (	INPUT: in DATA_IO;	-- input data in 24 bits range
								-- 12 bits Re, 12 bits Im
								-- (after they'll be converted into DATA_RDX inside this block)
			CK: in std_logic;	-- clock signal
			RES: in std_logic;	-- processor reset signal
			START: in std_logic;-- start/end data capture signal
			D_V: out std_logic;	-- data valid signal
			BUSY: out std_logic;-- processor unavailability signal
			OUTPUT: out DATA_IO	-- output data in 24 bits range (already converted inside this block)
								-- 12 bits Re, 12 bits Im
			);
end TOP;


architecture RTL of TOP is
	signal XIN: DATA_RDX;	-- inner signal for radix4 input
	signal XOUT: DATA_RDX;	-- inner signal for radix4 output
	signal CTR: std_logic;	-- inner signal of control among fsm and radix4
	
begin
	XIN <= (conv_signed(INPUT(1), 18), conv_signed(INPUT(0), 18));	-- conversion of input data (Re)
																	-- into a complex data DATA_RDX,
																	-- with Re part given by input
																	-- and Im part equals to 0

	OUTPUT <= (XOUT(1)(17 downto 6), XOUT(0)(17 downto 6));			-- conversion of output data of radix4
																	-- into the output data of 12 bits range.
																	-- the 12 bits most significant are taken,
																	-- so for getting the right value,
																	-- this output value must be muliplied by 2^6

-- FINITE STATE MACHINE
	FSM: entity WORK.FSM(RTL) port map (START => START, RES => RES, CK => CK, CTR => CTR, D_V => D_V, BUSY => BUSY);
-- RADIX4
	RDX4: entity WORK.RADIX4(RTL) port map (CK => CK, CTR => CTR, XIN => XIN, XOUT => XOUT);

end RTL;
