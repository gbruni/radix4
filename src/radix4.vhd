library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- computing part of fft-radix4 processor
-- it is controlled, using CTR,
-- by the finite state machine.
-- it's made up of port maps
-- of the various components

entity RADIX4 is

	port (	CK: in std_logic;	-- clock signal
			CTR: in std_logic;	-- enabling signal for computing ('1') and reset ('0')
			XIN: in DATA_RDX;	-- input data
			XOUT: out DATA_RDX	-- output data
		 );
end RADIX4;


architecture RTL of RADIX4 is
	attribute keep : boolean;
	
	------------------------------------ intermediate signals for blocks linking ------------------------------------
	signal SH10, SH11, SH12, SH21, SH22, SH23, SH30, SH31, SH32, SH41, SH42, SH43, SH50, SH51, SH52, SH61, SH62, SH63, SH70, SH71,SH72	: DATA_RDX;
	signal BF10, BF11, BF12, BF13, BF20, BF21, BF22, BF23, BF30, BF31, BF32, BF33														: DATA_RDX;
	signal DC10, DC11, DC12, DC13, DC20, DC21, DC22, DC23, DC30, DC31, DC32, DC33														: DATA_RDX;
	signal CM11, CM12, CM13, CM21, CM22, CM23																							: DATA_RDX;
	signal PS10, PS11, PS12, PS13																										: DATA_RDX;
	signal GND																															: DATA_RDX; -- ground signal
	
	attribute keep of XOUT : signal is true;
	attribute keep of PS10 : signal is true;
	attribute keep of PS11 : signal is true;
	attribute keep of PS12 : signal is true;
	attribute keep of PS13 : signal is true;

begin

	GND <= (conv_signed(0, 18), conv_signed(0, 18)); -- ground signal setting

-- INPUT DELAY COMMUTATOR
	DC1: entity WORK.DELAY_C(RTL) generic map (N => 6, DL => 0) port map (A0 => XIN, A1 => GND, A2 => GND, A3 => GND, CK => CK, CTR => CTR, Z0 => SH10, Z1 => SH11, Z2 => SH12, Z3 => BF13);

-------------------------------------------------- 1st part --------------------------------------------------
-- SHIFT REGISTERS
	SHR1: entity WORK.SFT_REG(RTL) generic map (N => 48) port map (A => SH10, CK => CK, CTR => CTR, Z => BF10);
	SHR2: entity WORK.SFT_REG(RTL) generic map (N => 32) port map (A => SH11, CK => CK, CTR => CTR, Z => BF11);
	SHR3: entity WORK.SFT_REG(RTL) generic map (N => 16) port map (A => SH12, CK => CK, CTR => CTR, Z => BF12);
-- BUTTERFLY
	BF1: entity WORK.BF(RTL) port map (A0 => BF10, A1 => BF11, A2 => BF12, A3 => BF13, Z0 => DC10, Z1 => CM11, Z2 => CM12, Z3 => CM13);
-- COMPLEX MULTIPLIERS
	CM1: entity WORK.COMPL_MUL(RTL64) generic map (ROW => 1) port map (A0 => CM11, CK => CK, CTR => CTR, Z0 => SH21);
	CM2: entity WORK.COMPL_MUL(RTL64) generic map (ROW => 2) port map (A0 => CM12, CK => CK, CTR => CTR, Z0 => SH22);
	CM3: entity WORK.COMPL_MUL(RTL64) generic map (ROW => 3) port map (A0 => CM13, CK => CK, CTR => CTR, Z0 => SH23);

-------------------------------------------------- 2nd part --------------------------------------------------
-- SHIFT REGISTERS
	SHR4: entity WORK.SFT_REG(RTL) generic map (N => 4) port map  (A => SH21, CK => CK, CTR => CTR, Z => DC11);
	SHR5: entity WORK.SFT_REG(RTL) generic map (N => 8) port map  (A => SH22, CK => CK, CTR => CTR, Z => DC12);
	SHR6: entity WORK.SFT_REG(RTL) generic map (N => 12) port map (A => SH23, CK => CK, CTR => CTR, Z => DC13);
-- DELAY COMMUTATOR
	DC2: entity WORK.DELAY_C(RTL) generic map (N => 4, DL => 0) port map (A0 => DC10, A1 => DC11, A2 => DC12, A3 => DC13, Z0 => SH30, Z1 => SH31, Z2 => SH32, Z3 => BF23, CK => CK, CTR => CTR);
-- SHIFT REGISTERS
	SHR7: entity WORK.SFT_REG(RTL) generic map (N => 12) port map	(A => SH30, CK => CK, CTR => CTR, Z => BF20);
	SHR8: entity WORK.SFT_REG(RTL) generic map (N => 8) port map	(A => SH31, CK => CK, CTR => CTR, Z => BF21);
	SHR9: entity WORK.SFT_REG(RTL) generic map (N => 4) port map	(A => SH32, CK => CK, CTR => CTR, Z => BF22);
-- BUTTERFLY
	BF2: entity WORK.BF(RTL) port map (A0 => BF20, A1 => BF21, A2 => BF22, A3 => BF23, Z0 => DC20, Z1 => CM21, Z2 => CM22, Z3 => CM23);
-- COMPLEX MULTIPLIERS
	CM4: entity WORK.COMPL_MUL(RTL16) generic map (ROW => 1) port map (A0 => CM21, CK => CK, CTR => CTR, Z0 => SH41);
	CM5: entity WORK.COMPL_MUL(RTL16) generic map (ROW => 2) port map (A0 => CM22, CK => CK, CTR => CTR, Z0 => SH42);
	CM6: entity WORK.COMPL_MUL(RTL16) generic map (ROW => 3) port map (A0 => CM23, CK => CK, CTR => CTR, Z0 => SH43);

------------------------------------------------- 3rd part -------------------------------------------------
-- SHIFT REGISTERS
	SHR10: entity WORK.SFT_REG(RTL) generic map (N => 1) port map (A => SH41, CK => CK, CTR => CTR, Z => DC21);
	SHR11: entity WORK.SFT_REG(RTL) generic map (N => 2) port map (A => SH42, CK => CK, CTR => CTR, Z => DC22);
	SHR12: entity WORK.SFT_REG(RTL) generic map (N => 3) port map (A => SH43, CK => CK, CTR => CTR, Z => DC23);
-- DELAY COMMUTATOR
	DC3: entity WORK.DELAY_C(RTL) generic map (N => 2, DL => 0) port map (A0 => DC20, A1 => DC21, A2 => DC22, A3 => DC23, Z0 => SH50, Z1 => SH51, Z2 => SH52, Z3 => BF33, CK => CK, CTR => CTR);
-- SHIFT REGISTERS
	SHR13: entity WORK.SFT_REG(RTL) generic map (N => 3) port map (A => SH50, CK => CK, CTR => CTR, Z => BF30);
	SHR14: entity WORK.SFT_REG(RTL) generic map (N => 2) port map (A => SH51, CK => CK, CTR => CTR, Z => BF31);
	SHR15: entity WORK.SFT_REG(RTL) generic map (N => 1) port map (A => SH52, CK => CK, CTR => CTR, Z => BF32);
-- BUTTERFLY
	BF3: entity WORK.BF(RTL) port map (A0 => BF30, A1 => BF31, A2 => BF32, A3 => BF33, Z0 => DC30, Z1 => SH61, Z2 => SH62, Z3 => SH63);

-------------------------------------------------- 4th part --------------------------------------------------
-- Data Re-sorting
-- SHIFT REGISTERS
	SHR16: entity WORK.SFT_REG(RTL) generic map (N => 4) port map (A => SH61, CK => CK, CTR => CTR, Z => DC31);
	SHR17: entity WORK.SFT_REG(RTL) generic map (N => 8) port map (A => SH62, CK => CK, CTR => CTR, Z => DC32);
	SHR18: entity WORK.SFT_REG(RTL) generic map (N => 12) port map (A => SH63, CK => CK, CTR => CTR, Z => DC33);
-- DELAY COMMUTATOR
	DC4: entity WORK.DELAY_C(RTL) generic map (N => 4, DL => 1) port map (A0 => DC30, A1 => DC31, A2 => DC32, A3 => DC33, Z0 => SH70, Z1 => SH71, Z2 => SH72, Z3 => PS13, CK => CK, CTR => CTR);
-- SHIFT REGISTERS
	SHR19: entity WORK.SFT_REG(RTL) generic map (N => 12) port map (A => SH70, CK => CK, CTR => CTR, Z => PS10);
	SHR20: entity WORK.SFT_REG(RTL) generic map (N => 8) port map (A => SH71, CK => CK, CTR => CTR, Z => PS11);
	SHR21: entity WORK.SFT_REG(RTL) generic map (N => 4) port map (A => SH72, CK => CK, CTR => CTR, Z => PS12);
-- Data Realising
-- PARALLEL to SERIES
	PS3: entity WORK.PS(RTL) port map (A0 => PS10, A1 => PS11, A2 => PS12, A3 => PS13, Z0 => XOUT, CK => CK, CTR => CTR);

end RTL;

