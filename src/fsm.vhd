library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- finite state machine
-- it manages the fft process working
-- 
-- states
-- 		IDLE:	waiting for data.
-- 				Enabling signals checked at
--				every clock positive edge
-- 		LOAD:	loading of data and operations execution.
-- 				in this state the output data can be released.
-- 				enabling signals are checked at the end
--				of block (64 data) loading (so every 64 clock cycle)
-- 		STEP:	if there aren't other input data
-- 				the pipeline must be emptied out
-- 				and then returning in IDLE
-- 
-- One clock cycle before the output data are released,
-- data valid signals (D_V) is set to '1'
--
-- 2 counters are used,
-- one for the managing of input data (e of signal START)
-- and the other for managing of data valid.
-- The latter is delayed of 12 clock cycle
-- with respect the first because of the presence
-- of final block for output re-sorting and serialization
-- 


entity FSM is

	port (	CK: in std_logic;		-- clock signal
			RES: in std_logic;		-- reset signal
			START: in std_logic;	-- enable signal for reading
			CTR: out std_logic;		-- control signal of radix4
			D_V: out std_logic;		-- data valid signal
			BUSY : out std_logic	-- signal for processor unavailable
		 );
end FSM;

architecture RTL of FSM is
	type STATE is (IDLE, LOAD, STEP);		-- data type for machine states
	signal PSTATE, NSTATE: STATE;			-- signals for present and next state
	signal NBUSY, ND_V, NCTR: std_logic;	-- signals for next values of BUSY, D_V, CTR
	signal CNT: unsigned(5 downto 0);		-- counter signals (from 0 to 63)
	signal CNT_DV: unsigned(6 downto 0);	-- D_V counter signal (from 0 to 75)
begin
	process	(PSTATE, RES, CNT, START)		-- the sensitivity to CNT allows to check
											-- all the signals at every clock positive edge
	begin
		case PSTATE is
------------------ IDLE ------------------
		when IDLE =>
			if RES = '0' then
				NSTATE	<= IDLE;
				ND_V	<= '0';
				NBUSY	<= '0';
				NCTR	<= '0';
			elsif START = '1' then
				NSTATE	<= LOAD;
				NBUSY	<= '0';
				ND_V	<= '0';
				NCTR	<= '1';
			else
				NSTATE	<= IDLE;
				NBUSY	<= '0';
				ND_V	<= '0';
				NCTR	<= '0';
			end if;
------------------ LOAD ------------------
		when LOAD =>
			if RES = '0' then
				NSTATE	<= IDLE;
				NBUSY	<= '0';
				ND_V	<= '0';
				NCTR	<= '0';
			elsif CNT = conv_unsigned(63, CNT'length) then	-- every 64 clock cycle fsm checks
															-- the value of START
				if START = '1' then
					NSTATE	<= LOAD;
					NBUSY	<= '0';
					-- memory for D_V
					NCTR	<= '1';
				else
					NSTATE	<= STEP;
					NBUSY	<= '1';
					-- memory for D_V
					NCTR	<= '1';
				end if;
			elsif CNT_DV = conv_unsigned(74, CNT_DV'length) then	-- after 75 clock cycle the fsm is in LOAD,
																	-- D_V is activated (set to '1') because
																	-- at the next clock cycle, data output
																	-- will be realised
				NSTATE	<= LOAD;
				NBUSY	<= '0';
				ND_V	<= '1';
				NCTR	<= '1';
			else
				NSTATE	<= LOAD;
				NBUSY	<= '0';
				-- memory for D_V
				NCTR	<= '1';
			end if;
------------------ STEP ------------------
		when STEP =>
			if RES = '0' then
				NSTATE	<= IDLE;
				NBUSY	<= '0';
				ND_V	<= '0';
				NCTR	<= '0';
			elsif CNT_DV = conv_unsigned(74, CNT_DV'length) then	-- after 75 clock cycle the fsm is
																	-- in STEP the pipeline is emptied out
				NSTATE	<= IDLE;
				NBUSY	<= '0';
				ND_V	<= '0';
				NCTR	<= '0';
			elsif CNT_DV < conv_unsigned(10, CNT_DV'length) then	-- for the first 11 cycles fsm
																	-- keeps the old value of D_V.
																	-- after 11 clock cycles D_V is
																	-- set to '1' (this is the case
																	-- when in input is given only 1 block
																	-- of 64 data and then stop,
																	-- otherwise D_V is already '1')
				NSTATE <= STEP;
				NBUSY	<= '1';
				-- ND_V	<= '1'; -- memory for D_V
				NCTR	<= '1';
			else																
				NSTATE	<= STEP;
				NBUSY	<= '1';
				ND_V	<= '1';
				NCTR	<= '1';
			end if;
		end case;
	end process;
	
	process(CK)	-- signals update in the clock positive edge
	begin
		if CK'event and CK='1' then
			PSTATE <= NSTATE;
			CTR <= NCTR;
			BUSY <= NBUSY;
			D_V <= ND_V;
		end if;
	end process;

	process(CK)	-- 2 counters
	begin
		if CK'event and CK='1' then
			if RES='0' or NSTATE = IDLE then				-- the counter reset is done both
															-- when there is an asynchronous reset
															-- and when the fsm goes to IDLE in the next
															-- clock cycle (see transition STEP -> IDLE)
				CNT <= conv_unsigned(63, CNT'length);		-- reset of 64-values counter
				CNT_DV <= conv_unsigned(127, CNT_DV'length);-- reset of 128-values counter (D_V)
			else
				CNT <= CNT+1;				-- counter CNT increasing

				if (CNT_DV = conv_unsigned(75, CNT_DV'length)) or (NSTATE = STEP and PSTATE = LOAD) then	-- reset of CNT_DV when it realises 75 OR
					CNT_DV <= conv_unsigned(0, CNT_DV'length);												--  even when fsm goes to STEP from LOAD
				else
					CNT_DV <= CNT_DV +1;	-- increasing of CNT_DV
				end if;
			end if;
		end if;
	end process;
	
end RTL;
