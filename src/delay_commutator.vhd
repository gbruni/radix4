library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- delay commutator
-- it distributes the data in the rows in such a way
-- to allow the computation on 64-points DFT.
-- there are 4 configurations for the mixing
-- and they are applied cyclically
-- CONF0 -> CONF1 -> CONF2 -> CONF3 -> CONF0 -> CONF1 -> ...
-- configurations are taken from the referential paper
--
-- the inner counter allows selecting
-- the configuration for the data mixing.
-- The counter overflow is not managed
-- to allow continuous and cyclically 
-- configurations selecting
--
-- the generics are used for
--		N:	set the bit length of counter,
--			and so setting the its values range.
--			for getting what value is right for N
-- 			just computing the next expression
--			4*duration_of_every_configuration
--			where duration_of_every_configuration is
--			expressed using the number of clock cycles
--		DL:	it's a delay for the counter when it is reset.
--			it is used especially in the last delay commutator
--			because the data arrival is delayed of 1 clock cycle
--
-- the counter reset takes place when CTR='0'
--

entity DELAY_C is
	generic(N, DL: natural);
	port (	A0, A1, A2, A3: in DATA_RDX;	-- input data
			CK: in std_logic;				-- clock signal
			CTR: in std_logic;				-- counter reset
			Z0, Z1, Z2, Z3: out DATA_RDX);	-- output data
end DELAY_C;


architecture RTL of DELAY_C is
	signal CNT: unsigned(N-1 downto 0);					-- counter signal (2^(N-1) values)
	signal CONF: std_logic_vector(1 downto 0);			-- configuration selected (0, 1, 2, 3)
	signal FLAG: std_logic_vector(2 downto 0);			-- flag for selecting the configuration:
														-- it's made up of CTR e CONF bits
	signal ZEROS: DATA_RDX;								-- signals for resetting of outputs
														-- when there is reset (CTR=0)
	type DATA_TEMP is array (3 downto 0) of DATA_RDX;	-- data type for the temporary managing of outputs
														-- depending on the chosen configuration
	signal XOUT: DATA_TEMP;								-- inner signal for managing the outputs
begin
	CONF <= CNT(CNT'left) & CNT (CNT'left-1);			-- the configuration is expressed
														-- straight by CNT
														-- (using the 2 most significant bits)

	FLAG <= CTR & CONF;
	ZEROS <= (conv_signed(0, Z0(0)'length) , conv_signed(0, Z0(1)'length)); -- signal for outputs reset
	
	Z0 <= XOUT(0); -- outputs setting
	Z1 <= XOUT(1); -- outputs setting
	Z2 <= XOUT(2); -- outputs setting
	Z3 <= XOUT(3); -- outputs setting
	
	with FLAG select	-- code for values setting in every outputs
		XOUT <=	(ZEROS, ZEROS, ZEROS, ZEROS)	when "000" | "001"	 | "010" | "011",	-- CTR = 0 implies outputs reset
				(A1, A2, A3, A0)				when "100",								-- configuration 0 (CTR=1)
				(A2, A3, A0, A1)				when "101",								-- configuration 1 (CTR=1)
				(A3, A0, A1, A2)				when "110",								-- configuration 2 (CTR=1)
				(A0, A1, A2, A3)				when "111",								-- configuration 3 (CTR=1)
				(ZEROS, ZEROS, ZEROS, ZEROS)	when others;							

	
	process(CK)	-- counter positive edge
	begin
		if CK'event and CK='1' then
			if CTR='0' then			-- counter reset to DL
				CNT <= conv_unsigned(DL, CNT'length);
			else
				CNT <= CNT+1;		-- counter increasing
			end if;
		end if;
	end process;

end RTL;

