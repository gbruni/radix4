library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- parallel to serial block for output
--
-- since the rate (of this block) of input data
-- is 4 times the output rate, we need of using memory
-- 
-- the memory, since it is managed in a intelligent way,
-- turns out to be smaller (49 DATA_RDX) than
-- that is requirement for storing every
-- data of a block (64 DATA_RDX).
-- all this is possible because while
-- the columns of data are arriving,
-- the data already arrived are realised
-- 
-- the first ever data are realised only
-- after 75 clock cycles and not after 64 clock
-- cycles because of the presence of
-- the extra block for re-sorting.
-- 
-- the counter is reset in such a way
-- to consider this latter fact.
-- 
-- the next first block data arrive
-- after every 64 clock cycles.
-- this block need always 16 clock cycles for
-- completing the arrival of an entire block (16x4=64).
-- so, there are 48 clock cycle where no data arrives.
-- the counter is needed for keeping track of
-- the missing data or how many clock clycles
-- there are for the next block data arrival
--
-- the memory control is made through
-- a modified shift register.
-- at every clock positive edge, data
-- are shifted forward of one position.
-- then 2 actions could be followed:
-- 		if there are other arriving data (0<CNT<15)
-- 			they are put in positions between 
-- 			3*CNT and 3*CNT+3 (4 positions)
-- 		if there are not arriving data (16<CNT<63)
-- 			the block must be emptied out and so
-- 			only the shifting action is necessary
-- 
-- in output at every clock cycle
-- the data in position 0 is put out
--


entity PS is

	port (	CK: in std_logic;			-- clock signal
			CTR: in std_logic;			-- enabling/reset signal
			A0, A1, A2, A3: in DATA_RDX;	-- input data
			Z0: out DATA_RDX				-- output data
		 );
end PS;

architecture RTL of PS is
	attribute keep : boolean;
	
	type sft is array (48 downto 0) of DATA_RDX;-- data type for shift register
	signal X: sft;								-- shift register
	signal CNT: unsigned(5 downto 0);			-- counter signal (from 0 to 63)
	
	attribute keep of X : signal is true;

begin

	Z0 <= X(0);	-- output of shift register

	process(CK)
		variable S: std_logic_vector(1 downto 0);	-- variable for managing the decision about the data insertion
		variable C: integer;						-- variable for allowing a better code reading
	begin
		if CK'event and CK='1' then
			if CTR='0' then	-- reset to 53 for keep track of the delay of the blocks data
				CNT <= conv_unsigned(53, CNT'length);
				X <= (others => (conv_signed(0, A0(0)'length) , conv_signed(0, A0(1)'length)));
			else
				CNT <= CNT+1;	-- counter increasing
				X(47 downto 0) <= X(48 downto 1);	-- shift
			end if;
	
			S := CNT(CNT'left) & CNT(CNT'left-1);
			if S="00" then	-- for 0<CNT<15, after the shifting, data insertion is performed
				C := 3*conv_integer(CNT);
				X(C) <= A0;		-- data placement in the shift register
				X(C+1) <= A1;
				X(C+2) <= A2;
				X(C+3) <= A3;
			end if;
		end if;
	end process;

end RTL;
