library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use WORK.radix4_pkg.all;

-- Shift Register positive edge trigger
-- it's generic: it contains N data of type DATA_RDX.
-- synchronous reset

entity SFT_REG is
	generic(N: natural);		-- defines the size of shift register
	port (	A: in DATA_RDX;		-- input data, stored in position N-1
			CK: in std_logic;	-- clock signal
			CTR: in std_logic;	-- enablin/resetting signal
								-- if CTR='0' => reset (to 0) the contents of every register
			Z: out DATA_RDX);	-- output data, it coincides with that is in position 0
end SFT_REG;


architecture RTL of SFT_REG is
	type sft is array (N-1 downto 0) of DATA_RDX;	-- data type for describing register
	signal X: sft;	-- shift register
begin
	process(CK)	-- process for setting and resetting of shift register
	begin
		if CK'event and CK='1' then
			if CTR='0' then	-- reset of shift registercontents
				X <= (others => (conv_signed(0, A(0)'length) , conv_signed(0, A(1)'length)));
			else			-- data shift, insertion and realising
				if not(N = 1) then -- particular case, when N=1
					X(N-2 downto 0) <= X(N-1 downto 1);
				end if;
				X(N-1) <= A;-- data insertion in position N-1
			end if;
		end if;
	end process;
	Z <= X(0);	-- realising of the data in position 0
end RTL;

