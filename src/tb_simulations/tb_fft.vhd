library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use STD.TEXTIO.all;
use WORK.radix4_pkg.all;


entity TB_FFT is
  
end TB_FFT;

architecture TEST of TB_FFT is

	signal XT : DATA_IO;
	signal YR : DATA_IO;
	signal CLK : std_logic := '0';
	signal RES : std_logic := '0';
	signal START : std_logic := '0';
	signal D_V, BUSY: std_logic;
	
begin  -- TEST

	  CLK <= not CLK after TCK/2 when now < TMAX; -- TCK e TMAX definiti nel package
	  RES <= '1' after 90 ns; -- viene tolto il reset

	-- carica i dati dal file creato con MATLAB
	process
		variable VLINE : line; -- variabili per la lettura dei dati
		variable V : integer;
		file INP_FILE : text open read_mode is "./src/fft_input.txt";
	begin
		while not(endfile(INP_FILE)) loop
				START <= '1'; -- attivazione lettura
			wait until CLK'event and CLK = '1';
			if RES = '1' then
				readline(INP_FILE, VLINE);
				read(VLINE, V);
				XT <= (conv_signed(V, 12), conv_signed(0, 12)); -- il segnale e' convertito in 12 bit
																-- e messo nella parte reale del DATA_IO
			end if;
		end loop;
		START <= '0'; -- disattivazione lettura
		wait;
	end process; 

	FFT_RADIX : entity WORK.TOP(RTL) port map (INPUT => XT, CK => CLK, RES => RES, START => START, D_V => D_V, BUSY => BUSY, OUTPUT => YR); -- portmap del componente

	-- salva i dati Y in un file per confrontarli con quelli
	-- ottenuti dal golden model MATLAB
	process
		variable WLINE : line;	-- variabili per la scrittura dei dati
		variable WVR, WVI, WM : integer;
		file OUT_FILE : text open write_mode is "./src/sim_yr.txt";
	begin
		wait until CLK'event and CLK = '1';
		if D_V = '1' then
			wait for OUTDEL;
			WVR := conv_integer(YR(1));
			WVI := conv_integer(YR(0));
			write(WLINE, integer'image(WVR) & " " & integer'image(WVI)); -- scrittura dei dati sul file
			writeline(OUT_FILE, WLINE); 
		end if;
	end process; 
end TEST;

