# Radix4 - A Fast Fourier Transform Processor

Circuit based on:
E. E. Swartzlander, W. W. Young, S. J. Joseph, **A Radix 4 Delay Commutator
for Fast Fourier Transform Processor Implementation**, IEEE Journal of
Solid-State Circuits, SC-19(5), pp. 702–709, Oct. 1984

For more information, check the file radix4_slides.pdf.
