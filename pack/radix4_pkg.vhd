library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;


-- Package for the FFT-RADIX4 processor. It contains:
					-- definition of data types
					-- constants for complex multiplier-twiddle factor (ROM)
					-- constants for simulation

package radix4_pkg is
	type DATA_RDX is array (1 downto 0) of signed(17 downto 0);		-- array of 2 18bits-signed.
																	-- it represents a complex number
																	-- DATA_RDX(1): Re part
																	-- DATA_RDX(0): Im part

	type DATA_IO is array (1 downto 0) of signed(11 downto 0);		-- array of 2 12bits-signed.
																	-- used for input and output of entire processor

----------------------- DATA TYPES (FOR TWIDDLE FACTOR) USED IN COMPLEX MULTIPLIERS OF DFT A 64 DATI (16 COLONNE) -----------------------
	type COEFF_TWDL64 is array (0 to 15) of signed(12 downto 0);	-- array of 16 13bits-signed.
																	-- it contains only one data series
																	-- useful to the complex multiplier in case of
																	-- multiplication with fixed twiddle factor

	type MATRIX_TWDL64 is array (2 downto 0) of COEFF_TWDL64;		-- array of 3 COEFF_TWDL.
																	-- it's a matrix made up of
																	-- 3 columns and 16 rows.
																	-- it contains all the 3 elements useful
																	-- to the complex multiplier in the case of
																	-- multiplication with given twiddle factor
																	-- 		MATRIX_TWDL(2):	Re parts WR of twiddle factor
																	-- 		MATRIX_TWDL(1):	sums WS of Re parts and
																	-- 						Im parts of twiddle factor
																	-- 		MATRIX_TWDL(0):	difference WD among Re parts
																	-- 						and Im parts of twiddle factor

	type CUBE_TWDL64 is array (1 to 3) of MATRIX_TWDL64;			-- cubic matrix (as array of array)
																	-- 		CUBE_TWDL(3): 	coefficients for complex
																	--						multiplier of row 3
																	-- 		CUBE_TWDL(2): 	coefficients for complex
																	--						multiplier of row 2
																	-- 		CUBE_TWDL(1): 	coefficients for complex
																	--						multiplier of row 1

----------------------- DATA TYPES (FOR TWIDDLE FACTOR) USED IN COMPLEX MULTIPLIERS OF DFT A 16 DATI (4 COLONNE) -----------------------
	type COEFF_TWDL16 is array (0 to 3) of signed(12 downto 0);		-- array of 16 13bits-signed.
																	-- it contains only one data series
																	-- useful to the complex multiplier in case of
																	-- multiplication with fixed twiddle factor

	type MATRIX_TWDL16 is array (2 downto 0) of COEFF_TWDL16;		-- array of 3 COEFF_TWDL.
																	-- it's a matrix made up of
																	-- 3 columns and 16 rows.
																	-- it contains all the 3 elements useful
																	-- to the complex multiplier in the case of
																	-- multiplication with given twiddle factor
																	-- 		MATRIX_TWDL(2):	Re parts WR of twiddle factor
																	-- 		MATRIX_TWDL(1):	sums WS of Re parts and
																	-- 						Im parts of twiddle factor
																	-- 		MATRIX_TWDL(0):	difference WD among Re parts
																	-- 						and Im parts of twiddle factor

	type CUBE_TWDL16 is array (1 to 3) of MATRIX_TWDL16;			-- cubic matrix (as array of array)
																	-- 		CUBE_TWDL(3): 	coefficients for complex
																	--						multiplier of row 3
																	-- 		CUBE_TWDL(2): 	coefficients for complex
																	--						multiplier of row 2
																	-- 		CUBE_TWDL(1): 	coefficients for complex
																	--						multiplier of row 1


	constant TWDL64 : CUBE_TWDL64 := 	(
										
										(-- TWDL64_ROW1 complex multiplier of row 1
										(-- WR
										conv_signed(2047, 13),
										conv_signed(2037, 13),
										conv_signed(2008, 13),
										conv_signed(1959, 13),
										conv_signed(1891, 13),
										conv_signed(1805, 13),
										conv_signed(1702, 13),
										conv_signed(1582, 13),
										conv_signed(1447, 13),
										conv_signed(1299, 13),
										conv_signed(1137, 13),
										conv_signed(965, 13),
										conv_signed(783, 13),
										conv_signed(594, 13),
										conv_signed(399, 13),
										conv_signed(201, 13)
										),
										(-- WS
										conv_signed(2047, 13),
										conv_signed(1837, 13),
										conv_signed(1608, 13),
										conv_signed(1365, 13),
										conv_signed(1108, 13),
										conv_signed(840, 13),
										conv_signed(565, 13),
										conv_signed(284, 13),
										conv_signed(0, 13),
										conv_signed(-284, 13),
										conv_signed(-565, 13),
										conv_signed(-840, 13),
										conv_signed(-1108, 13),
										conv_signed(-1365, 13),
										conv_signed(-1608, 13),
										conv_signed(-1837, 13)
										),
										(-- WD
										conv_signed(2047, 13),
										conv_signed(2238, 13),
										conv_signed(2407, 13),
										conv_signed(2553, 13),
										conv_signed(2675, 13),
										conv_signed(2770, 13),
										conv_signed(2839, 13),
										conv_signed(2881, 13),
										conv_signed(2895, 13),
										conv_signed(2881, 13),
										conv_signed(2839, 13),
										conv_signed(2770, 13),
										conv_signed(2675, 13),
										conv_signed(2553, 13),
										conv_signed(2407, 13),
										conv_signed(2238, 13)
										)
										),
										
										(-- TWDL64_ROW2 complex multiplier of row 2
										(-- WR
										conv_signed(2047, 13),
										conv_signed(2008, 13),
										conv_signed(1891, 13),
										conv_signed(1702, 13),
										conv_signed(1447, 13),
										conv_signed(1137, 13),
										conv_signed(783, 13),
										conv_signed(399, 13),
										conv_signed(0, 13),
										conv_signed(-399, 13),
										conv_signed(-783, 13),
										conv_signed(-1137, 13),
										conv_signed(-1447, 13),
										conv_signed(-1702, 13),
										conv_signed(-1891, 13),
										conv_signed(-2008, 13)
										),
										(-- WS
										conv_signed(2047, 13),
										conv_signed(1608, 13),
										conv_signed(1108, 13),
										conv_signed(565, 13),
										conv_signed(0, 13),
										conv_signed(-565, 13),
										conv_signed(-1108, 13),
										conv_signed(-1608, 13),
										conv_signed(-2047, 13),
										conv_signed(-2407, 13),
										conv_signed(-2675, 13),
										conv_signed(-2839, 13),
										conv_signed(-2895, 13),
										conv_signed(-2839, 13),
										conv_signed(-2675, 13),
										conv_signed(-2407, 13)
										),
										(-- WD
										conv_signed(2047, 13),
										conv_signed(2407, 13),
										conv_signed(2675, 13),
										conv_signed(2839, 13),
										conv_signed(2895, 13),
										conv_signed(2839, 13),
										conv_signed(2675, 13),
										conv_signed(2407, 13),
										conv_signed(2047, 13),
										conv_signed(1608, 13),
										conv_signed(1108, 13),
										conv_signed(565, 13),
										conv_signed(0, 13),
										conv_signed(-565, 13),
										conv_signed(-1108, 13),
										conv_signed(-1608, 13)
										)
										),
										
										(-- TWDL64_ROW3 complex multiplier of row 3
										(-- WR
										conv_signed(2047, 13),
										conv_signed(1959, 13),
										conv_signed(1702, 13),
										conv_signed(1299, 13),
										conv_signed(783, 13),
										conv_signed(201, 13),
										conv_signed(-399, 13),
										conv_signed(-965, 13),
										conv_signed(-1447, 13),
										conv_signed(-1805, 13),
										conv_signed(-2008, 13),
										conv_signed(-2037, 13),
										conv_signed(-1891, 13),
										conv_signed(-1582, 13),
										conv_signed(-1137, 13),
										conv_signed(-594, 13)
										),
										(-- WS
										conv_signed(2047, 13),
										conv_signed(1365, 13),
										conv_signed(565, 13),
										conv_signed(-284, 13),
										conv_signed(-1108, 13),
										conv_signed(-1837, 13),
										conv_signed(-2407, 13),
										conv_signed(-2770, 13),
										conv_signed(-2895, 13),
										conv_signed(-2770, 13),
										conv_signed(-2407, 13),
										conv_signed(-1837, 13),
										conv_signed(-1108, 13),
										conv_signed(-284, 13),
										conv_signed(565, 13),
										conv_signed(1365, 13)
										),
										(-- WD
										conv_signed(2047, 13),
										conv_signed(2553, 13),
										conv_signed(2839, 13),
										conv_signed(2881, 13),
										conv_signed(2675, 13),
										conv_signed(2238, 13),
										conv_signed(1608, 13),
										conv_signed(840, 13),
										conv_signed(0, 13),
										conv_signed(-840, 13),
										conv_signed(-1608, 13),
										conv_signed(-2238, 13),
										conv_signed(-2675, 13),
										conv_signed(-2881, 13),
										conv_signed(-2839, 13),
										conv_signed(-2553, 13)
										)
										)
										
										);


	constant TWDL16 : CUBE_TWDL16 := 	(
										
										(-- TWDL16_ROW1 complex multiplier of row 1
										(-- WR
										conv_signed(2047, 13),
										conv_signed(1891, 13),
										conv_signed(1447, 13),
										conv_signed(783, 13)
										),
										(-- WS
										conv_signed(2047, 13),
										conv_signed(1108, 13),
										conv_signed(0, 13),
										conv_signed(-1108, 13)
										),
										(-- WD
										conv_signed(2047, 13),
										conv_signed(2675, 13),
										conv_signed(2895, 13),
										conv_signed(2675, 13)
										)
										),
										
										(-- TWDL16_ROW2 complex multiplier of row 2
										(-- WR
										conv_signed(2047, 13),
										conv_signed(1447, 13),
										conv_signed(0, 13),
										conv_signed(-1447, 13)
										),
										(-- WS
										conv_signed(2047, 13),
										conv_signed(0, 13),
										conv_signed(-2047, 13),
										conv_signed(-2895, 13)
										),
										(-- WD
										conv_signed(2047, 13),
										conv_signed(2895, 13),
										conv_signed(2047, 13),
										conv_signed(0, 13)
										)
										),
										
										(-- TWDL16_ROW3 complex multiplier of row 3
										(-- WR
										conv_signed(2047, 13),
										conv_signed(783, 13),
										conv_signed(-1447, 13),
										conv_signed(-1891, 13)
										),
										(-- WS
										conv_signed(2047, 13),
										conv_signed(-1108, 13),
										conv_signed(-2895, 13),
										conv_signed(-1108, 13)
										),
										(-- WD
										conv_signed(2047, 13),
										conv_signed(2675, 13),
										conv_signed(0, 13),
										conv_signed(-2675, 13)
										)
										)
										
										);

------------------------------------ INPUT DATA - MEMORY ------------------------------------

	type DATA_INPUT is array (0 to 127) of integer;	-- data type for giving an input to the processor
													-- without reading from an external source

	constant INPUT_FFT : DATA_INPUT :=	(	-- 128 data for input
										0,
										205,
										84,
										86,
										43,
										131,
										-58,
										92,
										-28,
										-205,
										-19,
										-451,
										-198,
										-227,
										-65,
										-715,
										485,
										1679,
										408,
										40,
										-410,
										183,
										-553,
										442,
										57,
										-562,
										99,
										-1080,
										-86,
										85,
										480,
										-1140,
										639,
										2046,
										-447,
										-872,
										-971,
										417,
										-28,
										1189,
										596,
										-251,
										167,
										-839,
										-30,
										55,
										164,
										-994,
										-97,
										618,
										-394,
										-281,
										-77,
										494,
										261,
										446,
										127,
										-147,
										-62,
										-213,
										-39,
										-14,
										0,
										-164,
										0,
										205,
										84,
										86,
										43,
										131,
										-58,
										92,
										-28,
										-205,
										-19,
										-451,
										-198,
										-227,
										-65,
										-715,
										485,
										1679,
										408,
										40,
										-410,
										183,
										-553,
										442,
										57,
										-562,
										99,
										-1080,
										-86,
										85,
										480,
										-1140,
										639,
										2046,
										-447,
										-872,
										-971,
										417,
										-28,
										1189,
										596,
										-251,
										167,
										-839,
										-30,
										55,
										164,
										-994,
										-97,
										618,
										-394,
										-281,
										-77,
										494,
										261,
										446,
										127,
										-147,
										-62,
										-213,
										-39,
										-14,
										0,
										-164
										);
										
	type ERR_VEC is array (INPUT_FFT'range) of std_logic_vector (2 downto 0);	-- INPUT_FFT'range is 0 to N

---------------------------- constants for simulation - testbench ----------------------------
	constant TCK : time := 50 ns;		-- clock period
	constant TCLK : time := 10 ns;		-- clock period for Lucas' work
	constant OUTDEL : time := 10 ns;	-- delay for sampling the output
	constant TMAX : time := 30000 ns;	-- maximum simulation length

end radix4_pkg;

package body radix4_pkg is
end;
